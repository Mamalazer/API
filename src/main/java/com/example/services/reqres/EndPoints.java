package com.example.services.reqres;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author IPetrov
 * @since 21.09.2021
 */
@AllArgsConstructor
@Getter
public enum EndPoints {

    BASE_URL(
            "Base API URL reqres.in",
            "https://reqres.in/api/"
    ),

    USERS(
            "EndPoint - \"users\"",
            "users"
    ),

    REGISTER(
            "EndPoint - \"register\"",
                    "register"
    ),

    LOGIN(
            "EndPoint - \"login\"",
            "login"
    );

    private final String description;
    private final String value;
}
