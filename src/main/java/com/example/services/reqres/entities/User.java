package com.example.services.reqres.entities;

import com.example.helper.BaseObject;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author IPetrov
 * @since 21.09.2021
 */
@Getter
@Setter
@JsonPropertyOrder(alphabetic = true)
public class User extends BaseObject {
    private int id;
    private String email;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    private String avatar;
}
