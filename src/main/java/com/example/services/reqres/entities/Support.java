package com.example.services.reqres.entities;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author IPetrov
 * @since 21.09.2021
 */
@Getter
@Setter
@JsonPropertyOrder({"url", "text"})
public class Support {
    private String url;
    private String text;
}
