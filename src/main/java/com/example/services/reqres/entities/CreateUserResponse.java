package com.example.services.reqres.entities;

import com.example.helper.BaseObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@JsonPropertyOrder(alphabetic = true)
public class CreateUserResponse extends BaseObject {

        private String name;
        private String job;
        private String id;
        private String createdAt;
//        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.SSSZ")
//        private LocalDateTime createdAt;

}
