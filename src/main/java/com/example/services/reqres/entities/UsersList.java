package com.example.services.reqres.entities;

import com.example.helper.BaseObject;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author IPetrov
 * @since 21.09.2021
 */
@Getter
@Setter
@JsonPropertyOrder({"page", "per_page", "total", "total_pages", "data"})
public class UsersList extends BaseObject {
    private int page;
    @JsonProperty("per_page")
    private int perPage;
    private int total;
    @JsonProperty("total_pages")
    private int totalPages;
    private List<User> data;
    private Support support;
}
