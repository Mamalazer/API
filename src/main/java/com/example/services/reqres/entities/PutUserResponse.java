package com.example.services.reqres.entities;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonPropertyOrder(alphabetic = true)
public class PutUserResponse {

    private String name;
    private String job;
    private String updatedAt;

}
