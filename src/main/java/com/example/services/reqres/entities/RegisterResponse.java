package com.example.services.reqres.entities;

import com.example.helper.BaseObject;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonPropertyOrder(alphabetic = true)
public class RegisterResponse extends BaseObject {

        private int id;
        private String token;

}
