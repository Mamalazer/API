package com.example.helper;

import static com.example.helper.logger.MessageFormatter.toJsonFormat;


/**
 * @author I.Petrov
 * @since 07.09.2020
 */
public class BaseObject {

    @Override
    public String toString() {
        return toJsonFormat(this);
    }
}
