package com.example.helper.rest.spec;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.authentication.AuthenticationScheme;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

import static com.example.helper.AllureAttachment.attachToAllureTextMessage;
import static io.restassured.http.ContentType.JSON;

/**
 * @author IPetrov
 * @since 21.09.2021
 */
public class Specification {

    // region AuthenticationScheme
    private static AuthenticationScheme getBasicAuthSchemeWithAllureAttaching(String login, String password) {
        PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
        authScheme.setUserName(login);
        authScheme.setPassword(password);

        attachToAllureTextMessage("Login", login);

        return authScheme;
    }

    private static AuthenticationScheme getBasicAuthScheme(String login, String password) {
        PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
        authScheme.setUserName(login);
        authScheme.setPassword(password);

        return authScheme;
    }
    // endregion AuthenticationScheme

    // region RequestSpecification for "service"
    public static RequestSpecification getServiceRequestSpecification(String baseUrl) {
        return new RequestSpecBuilder()
                .setBaseUri(baseUrl)
                .setContentType(JSON)
                .addFilter(new AllureRestAssured())
                .build();
    }

    public static RequestSpecification getServiceRequestSpecification(String baseUrl, AuthenticationScheme authScheme) {
        return new RequestSpecBuilder()
                .setBaseUri(baseUrl)
                .setAuth(authScheme)
                .setContentType(JSON)
                .addFilter(new AllureRestAssured())
                .build();
    }
    // region RequestSpecification for "service"

    public static RequestSpecification postServiceRequestSpecification(String baseUrl) {
        return new RequestSpecBuilder()
                .setBaseUri(baseUrl)
                .setContentType(JSON)
                .addFilter(new AllureRestAssured())
                .build();
    }

    public static RequestSpecification putServiceRequestSpecification(String baseUrl) {
        return new RequestSpecBuilder()
                .setBaseUri(baseUrl)
                .setContentType(JSON)
                .addFilter(new AllureRestAssured())
                .build();
    }

}
