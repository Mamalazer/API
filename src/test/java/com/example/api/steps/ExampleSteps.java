package com.example.api.steps;

import io.qameta.allure.Step;

public class ExampleSteps {

    @Step("firstStep")
    public static void firstStep(){
        System.out.println("firstStep");
    }

    @Step("secondStep")
    public static void secondStep(){
        System.out.println("secondStep");
    }

    @Step("thirdStep")
    public static void thirdStep(){
        System.out.println("thirdStep");
    }
}
