package com.example.api.steps.reqes;

import com.example.services.reqres.entities.PutUserResponse;
import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.SoftAssertions;

import java.time.LocalDate;

import static com.example.helper.AllureAttachment.attachToAllureJsonPathWithPrettify;
import static com.example.helper.date.DateTimeUtil.getStringFromDateTimeUTC;
import static com.example.helper.logger.MessageFormatter.format;
import static com.example.helper.mapping.JsonPathMapping.convertJsonPathToAnyObject;
import static com.example.helper.rest.HttpCode.OK;
import static com.example.helper.rest.request.PutRequest.doHttpPutRequest;
import static com.example.helper.rest.spec.Specification.postServiceRequestSpecification;
import static com.example.services.reqres.EndPoints.BASE_URL;
import static com.example.services.reqres.EndPoints.USERS;

public class PutUserSteps {

    private static final Logger LOGGER = LogManager.getLogger(PutUserSteps.class);

    private final static String SINGLE_USER_PARAMETERS = "{}/{}";

    @Step("Получить пользователя по id")
    public static PutUserResponse putUser(String name, String job) {
        String path = format(SINGLE_USER_PARAMETERS, USERS.getValue(), 2);
        String req = "{\n" +
                "    \"name\": \"" + name + "\",\n" +
                "    \"job\": \"" + job + "\"\n" +
                "}";

        JsonPath jsonPath = doHttpPutRequest(
                postServiceRequestSpecification(BASE_URL.getValue()),
                path,
                req,
                OK
        );

        attachToAllureJsonPathWithPrettify(PutUserResponse.class.getSimpleName(), jsonPath);

        PutUserResponse putUserResponse = convertJsonPathToAnyObject(jsonPath, PutUserResponse.class);
        LOGGER.debug("User from {}\n{}", getStringFromDateTimeUTC(), putUserResponse.toString());

        return putUserResponse;

    }

    @Step
    public static void assertPutUserResponse(PutUserResponse putUserResponse, String name, String job) {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(putUserResponse.getName())
                .as("Name: " + name)
                .isEqualTo(name);

        softly.assertThat(putUserResponse.getJob())
                .as("Job: " + job)
                .isEqualTo(job);

        softly.assertThat(putUserResponse.getUpdatedAt())
                .as("Time of creation")
                .contains(LocalDate.now().toString());

        softly.assertAll();
    }

}
