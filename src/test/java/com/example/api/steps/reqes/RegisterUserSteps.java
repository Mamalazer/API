package com.example.api.steps.reqes;

import com.example.services.reqres.entities.ErrorResponse;
import com.example.services.reqres.entities.RegisterResponse;
import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.SoftAssertions;

import static com.example.helper.AllureAttachment.attachToAllureJsonPathWithPrettify;
import static com.example.helper.date.DateTimeUtil.getStringFromDateTimeUTC;
import static com.example.helper.mapping.JsonPathMapping.convertJsonPathToAnyObject;
import static com.example.helper.rest.HttpCode.BAD_REQUEST;
import static com.example.helper.rest.HttpCode.OK;
import static com.example.helper.rest.request.PostRequest.doHttpPostRequest;
import static com.example.helper.rest.spec.Specification.postServiceRequestSpecification;
import static com.example.services.reqres.EndPoints.BASE_URL;
import static com.example.services.reqres.EndPoints.REGISTER;

public class RegisterUserSteps {

    private static final Logger LOGGER = LogManager.getLogger(RegisterUserSteps.class);

    @Step("Регистрация нового пользователя")
    public static RegisterResponse registerUser(String email, String password) {
        String path = REGISTER.getValue();
        String req = "{\n" +
                "    \"email\": \"" + email + "\",\n" +
                "    \"password\": \"" + password + "\"\n" +
                "}";

        JsonPath jsonPath = doHttpPostRequest(
                postServiceRequestSpecification(BASE_URL.getValue()),
                path,
                req,
                OK
        );

        attachToAllureJsonPathWithPrettify(RegisterResponse.class.getSimpleName(), jsonPath);

        RegisterResponse registerResponse = convertJsonPathToAnyObject(jsonPath, RegisterResponse.class);
        LOGGER.debug("User from {}\n{}", getStringFromDateTimeUTC(), registerResponse.toString());

        return registerResponse;

    }

    @Step
    public static void assertRegisterUserResponse(RegisterResponse registerResponse) {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(registerResponse.getId())
                .as("user id")
                .isEqualTo(4);

        softly.assertThat(registerResponse.getToken())
                .as("user's token")
                .isEqualTo("QpwL5tke4Pnpja7X4");

        softly.assertAll();
    }

    @Step("Регистрация нового пользователя без указания пароля")
    public static ErrorResponse registerUser(String email) {
        String path = REGISTER.getValue();
        String req = "{\n" +
                "    \"email\": \"" + email + "\"" +
                "}";

        JsonPath jsonPath = doHttpPostRequest(
                postServiceRequestSpecification(BASE_URL.getValue()),
                path,
                req,
                BAD_REQUEST
        );

        attachToAllureJsonPathWithPrettify(ErrorResponse.class.getSimpleName(), jsonPath);

        ErrorResponse errorResponse = convertJsonPathToAnyObject(jsonPath, ErrorResponse.class);
        LOGGER.debug("User from {}\n{}", getStringFromDateTimeUTC(), errorResponse.toString());

        return errorResponse;

    }

    @Step
    public static void assertNegativeRegisterUserResponse(ErrorResponse errorResponse) {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(errorResponse.getError())
                .as("error")
                .isEqualTo("Missing password");

        softly.assertAll();
    }

}
