package com.example.api.steps.reqes;

import com.example.services.reqres.entities.UsersList;
import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.SoftAssertions;

import static com.example.helper.AllureAttachment.attachToAllureJsonPathWithPrettify;
import static com.example.helper.date.DateTimeUtil.getStringFromDateTimeUTC;
import static com.example.helper.logger.MessageFormatter.format;
import static com.example.helper.mapping.JsonPathMapping.convertJsonPathToAnyObject;
import static com.example.helper.rest.HttpCode.OK;
import static com.example.helper.rest.request.GetRequest.doHttpGetRequest;
import static com.example.helper.rest.spec.Specification.getServiceRequestSpecification;
import static com.example.services.reqres.EndPoints.BASE_URL;
import static com.example.services.reqres.EndPoints.USERS;

/**
 * @author IPetrov
 * @since 21.09.2021
 */
public class UsersSteps {
    private static final Logger LOGGER = LogManager.getLogger(UsersSteps.class);

    private final static String USERS_PARAMETERS = "{}?page={}";
    private final static String SINGLE_USER_PARAMETERS = "{}/{}"; //api/users/2


    @Step("Получить список пользователей")
    public static UsersList getUserListStep(int page) {
        String path = format(USERS_PARAMETERS, USERS.getValue(), page);

        LOGGER.info("GET REQUEST -> {}{}", BASE_URL.getValue(), path);

        JsonPath jsonPath = doHttpGetRequest(
                getServiceRequestSpecification(BASE_URL.getValue()),
                path,
                OK
        );

        attachToAllureJsonPathWithPrettify(UsersList.class.getSimpleName(), jsonPath);

        UsersList usersList = convertJsonPathToAnyObject(jsonPath, UsersList.class);
        LOGGER.debug("UsersList from {}\n{}", getStringFromDateTimeUTC(), usersList.toString());

        return usersList;
    }

    @Step
    public static void assertUsersList(UsersList usersList) {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(usersList)
                .as("UsersList is not null")
                .isNotNull();
        softly.assertThat(usersList.getData().size())
                .as("UsersList.data.size > 0")
                .isGreaterThan(0);

        softly.assertAll();
    }

}
