package com.example.api.steps.reqes;

import com.example.services.reqres.entities.UserResponse;
import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.SoftAssertions;

import static com.example.helper.AllureAttachment.attachToAllureJsonPathWithPrettify;
import static com.example.helper.date.DateTimeUtil.getStringFromDateTimeUTC;
import static com.example.helper.logger.MessageFormatter.format;
import static com.example.helper.mapping.JsonPathMapping.convertJsonPathToAnyObject;
import static com.example.helper.rest.HttpCode.NOT_FOUND;
import static com.example.helper.rest.HttpCode.OK;
import static com.example.helper.rest.request.GetRequest.doHttpGetRequest;
import static com.example.helper.rest.spec.Specification.getServiceRequestSpecification;
import static com.example.services.reqres.EndPoints.BASE_URL;
import static com.example.services.reqres.EndPoints.USERS;

/**
 * @author IPetrov
 * @since 21.09.2021
 */
public class UserResponseSteps {
    private static final Logger LOGGER = LogManager.getLogger(UserResponseSteps.class);

    private final static String SINGLE_USER_PARAMETERS = "{}/{}"; //api/users/2


    @Step("Получить пользователя по id")
    public static UserResponse getSingleUser(String userId) {
        String path = format(SINGLE_USER_PARAMETERS, USERS.getValue(), userId);

        LOGGER.info("GET REQUEST -> {}{}", BASE_URL.getValue(), userId);

        JsonPath jsonPath = doHttpGetRequest(
                getServiceRequestSpecification(BASE_URL.getValue()),
                path,
                OK
        );

        attachToAllureJsonPathWithPrettify(UserResponse.class.getSimpleName(), jsonPath);

        UserResponse userResponse = convertJsonPathToAnyObject(jsonPath, UserResponse.class);
        LOGGER.debug("User from {}\n{}", getStringFromDateTimeUTC(), userResponse.toString());

        return userResponse;

    }

    @Step
    public static void assertUserResponse(UserResponse userResponse) {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(userResponse.getData().getId())
                .as("Id = 2")
                .isEqualTo(2);

        softly.assertThat(userResponse.getData().getEmail())
                .as("Email equal to janet.weaver@reqres.in")
                .isEqualTo("janet.weaver@reqres.in");

        softly.assertThat(userResponse.getData().getFirstName())
                .as("First name equal to Janet")
                .isEqualTo("Janet");

        softly.assertThat(userResponse.getData().getLastName())
                .as("Last name equal to Weaver")
                .isEqualTo("Weaver");

        softly.assertAll();
    }

    @Step("Получить несуществующего пользователя по id")
    public static UserResponse getSingleUserNegative(String userId) {
        String path = format(SINGLE_USER_PARAMETERS, USERS.getValue(), userId);

        LOGGER.info("GET REQUEST -> {}{}", BASE_URL.getValue(), userId);

        JsonPath jsonPath = doHttpGetRequest(
                getServiceRequestSpecification(BASE_URL.getValue()),
                path,
                NOT_FOUND
        );

        attachToAllureJsonPathWithPrettify(UserResponse.class.getSimpleName(), jsonPath);

        UserResponse userResponse = convertJsonPathToAnyObject(jsonPath, UserResponse.class);
        LOGGER.debug("User from {}\n{}", getStringFromDateTimeUTC(), userResponse.toString());

        return userResponse;

    }

    @Step
    public static void assertUserNegativeResponse(UserResponse userResponse) {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(userResponse.getData()).isNull();
        softly.assertThat(userResponse.getSupport()).isNull();

        softly.assertAll();
    }

}
