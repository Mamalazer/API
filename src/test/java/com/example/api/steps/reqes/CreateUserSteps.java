package com.example.api.steps.reqes;

import com.example.services.reqres.entities.CreateUserResponse;
import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.SoftAssertions;

import java.time.LocalDate;

import static com.example.helper.AllureAttachment.attachToAllureJsonPathWithPrettify;
import static com.example.helper.date.DateTimeUtil.getStringFromDateTimeUTC;
import static com.example.helper.mapping.JsonPathMapping.convertJsonPathToAnyObject;
import static com.example.helper.rest.HttpCode.CREATED;
import static com.example.helper.rest.request.PostRequest.doHttpPostRequest;
import static com.example.helper.rest.spec.Specification.postServiceRequestSpecification;
import static com.example.services.reqres.EndPoints.BASE_URL;
import static com.example.services.reqres.EndPoints.USERS;

public class CreateUserSteps {

    private static final Logger LOGGER = LogManager.getLogger(CreateUserSteps.class);

    @Step("Получить пользователя по id")
    public static CreateUserResponse createUser(String name, String job) {
        String path = USERS.getValue();
        String req = "{\n" +
                "    \"name\": \"" + name + "\",\n" +
                "    \"job\": \"" + job + "\"\n" +
                "}";

        JsonPath jsonPath = doHttpPostRequest(
                postServiceRequestSpecification(BASE_URL.getValue()),
                path,
                req,
                CREATED
        );

        attachToAllureJsonPathWithPrettify(CreateUserResponse.class.getSimpleName(), jsonPath);

        CreateUserResponse createUserResponse = convertJsonPathToAnyObject(jsonPath, CreateUserResponse.class);
        LOGGER.debug("User from {}\n{}", getStringFromDateTimeUTC(), createUserResponse.toString());

        return createUserResponse;

    }

    @Step
    public static void assertPositiveCreateUserResponse(CreateUserResponse createUserResponse, String name, String job) {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(createUserResponse.getName())
                .as("Name: " + name)
                .isEqualTo(name);

        softly.assertThat(createUserResponse.getJob())
                .as("Job: " + job)
                .isEqualTo(job);

        softly.assertThat(createUserResponse.getCreatedAt())
                .as("Time of creation")
                .contains(LocalDate.now().toString());

        softly.assertThat(createUserResponse.getId())
                .as("Id of new user")
                .isNotNull();

        softly.assertAll();
    }

}
