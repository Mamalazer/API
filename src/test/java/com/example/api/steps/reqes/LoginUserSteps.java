package com.example.api.steps.reqes;

import com.example.services.reqres.entities.ErrorResponse;
import com.example.services.reqres.entities.LoginResponse;
import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.SoftAssertions;

import static com.example.helper.AllureAttachment.attachToAllureJsonPathWithPrettify;
import static com.example.helper.date.DateTimeUtil.getStringFromDateTimeUTC;
import static com.example.helper.mapping.JsonPathMapping.convertJsonPathToAnyObject;
import static com.example.helper.rest.HttpCode.BAD_REQUEST;
import static com.example.helper.rest.HttpCode.OK;
import static com.example.helper.rest.request.PostRequest.doHttpPostRequest;
import static com.example.helper.rest.spec.Specification.postServiceRequestSpecification;
import static com.example.services.reqres.EndPoints.BASE_URL;
import static com.example.services.reqres.EndPoints.LOGIN;

public class LoginUserSteps {

    private static final Logger LOGGER = LogManager.getLogger(LoginUserSteps.class);

    @Step("Авторизация пользователя")
    public static LoginResponse login(String email, String password) {
        String path = LOGIN.getValue();
        String req = "{\n" +
                "    \"email\": \"" + email + "\",\n" +
                "    \"password\": \"" + password + "\"\n" +
                "}";

        JsonPath jsonPath = doHttpPostRequest(
                postServiceRequestSpecification(BASE_URL.getValue()),
                path,
                req,
                OK
        );

        attachToAllureJsonPathWithPrettify(LoginResponse.class.getSimpleName(), jsonPath);

        LoginResponse loginResponse = convertJsonPathToAnyObject(jsonPath, LoginResponse.class);
        LOGGER.debug("User from {}\n{}", getStringFromDateTimeUTC(), loginResponse.toString());

        return loginResponse;

    }

    @Step
    public static void assertLoginResponse(LoginResponse loginResponse) {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(loginResponse.getToken())
                .as("user's token")
                .isEqualTo("QpwL5tke4Pnpja7X4");

        softly.assertAll();
    }

    @Step("Авторизация пользователя без пароля")
    public static ErrorResponse login(String email) {
        String path = LOGIN.getValue();
        String req = "{\n" +
                "    \"email\": \"" + email + "\"" +
                "}";

        JsonPath jsonPath = doHttpPostRequest(
                postServiceRequestSpecification(BASE_URL.getValue()),
                path,
                req,
                BAD_REQUEST
        );

        attachToAllureJsonPathWithPrettify(ErrorResponse.class.getSimpleName(), jsonPath);

        ErrorResponse errorResponse = convertJsonPathToAnyObject(jsonPath, ErrorResponse.class);
        LOGGER.debug("User from {}\n{}", getStringFromDateTimeUTC(), errorResponse.toString());

        return errorResponse;

    }

    @Step
    public static void assertNegativeLoginResponse(ErrorResponse errorResponse) {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(errorResponse.getError())
                .as("error")
                .isEqualTo("Missing password");

        softly.assertAll();
    }

}
