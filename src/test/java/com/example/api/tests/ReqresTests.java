package com.example.api.tests;

import com.example.services.reqres.entities.*;
import io.qameta.allure.Description;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.example.api.steps.reqes.CreateUserSteps.assertPositiveCreateUserResponse;
import static com.example.api.steps.reqes.CreateUserSteps.createUser;
import static com.example.api.steps.reqes.LoginUserSteps.*;
import static com.example.api.steps.reqes.PutUserSteps.assertPutUserResponse;
import static com.example.api.steps.reqes.PutUserSteps.putUser;
import static com.example.api.steps.reqes.RegisterUserSteps.*;
import static com.example.api.steps.reqes.UserResponseSteps.*;
import static com.example.api.steps.reqes.UsersSteps.assertUsersList;
import static com.example.api.steps.reqes.UsersSteps.getUserListStep;

/**
 * @author IPetrov
 * @since 21.09.2021
 */
public class ReqresTests {

    @Test
    @DisplayName("Reqres firstTest name")
    @Description("Reqres firstTest description")
    public void firstTest() {
        UsersList usersListFirst = getUserListStep(1);
        assertUsersList(usersListFirst);

        UsersList usersListSecond = getUserListStep(2);
        assertUsersList(usersListSecond);
    }

    @Test
    @DisplayName("Reqres getUserForIdPositive test")
    @Description("Positive test on getting user for id")
    public void getUserForIdPositive() {
        UserResponse userResponse = getSingleUser("2");
        assertUserResponse(userResponse);
    }

    @Test
    @DisplayName("Reqres getUserForIdNegative test")
    @Description("Negative test on getting user for id")
    public void getUserForIdNegative() {
        UserResponse userResponse = getSingleUserNegative("999");
        assertUserNegativeResponse(userResponse);
    }

    @Test
    @DisplayName("Reqres createUserPositive test")
    @Description("Positive test for create user")
    public void createUserPositive() {
        CreateUserResponse createUserResponse = createUser("morpheus", "leader");
        assertPositiveCreateUserResponse(createUserResponse, "morpheus", "leader");
    }

    @Test
    @DisplayName("Reqres putUserPositive test")
    @Description("Positive test for put user")
    public void putUserPositive() {
        PutUserResponse putUserResponse = putUser("morpheus", "zion resident");
        assertPutUserResponse(putUserResponse, "morpheus", "zion resident");
    }

    @Test
    @DisplayName("Reqres registerUserPositive test")
    @Description("Positive test for registration user")
    public void registerUserPositive() {
        RegisterResponse registerResponse = registerUser("eve.holt@reqres.in", "pistol");
        assertRegisterUserResponse(registerResponse);
    }

    @Test
    @DisplayName("Reqres registerUserNegative test")
    @Description("Negative test for registration user without password")
    public void registerUserNegative() {
        ErrorResponse errorResponse = registerUser("eve.holt@reqres.in");
        assertNegativeRegisterUserResponse(errorResponse);
    }

    @Test
    @DisplayName("Reqres loginUserPositive test")
    @Description("Positive login")
    public void loginUserPositive() {
        LoginResponse loginResponse = login("eve.holt@reqres.in", "cityslicka");
        assertLoginResponse(loginResponse);
    }

    @Test
    @DisplayName("Reqres loginUserPositive test")
    @Description("Positive login")
    public void loginUserNegative() {
        ErrorResponse errorResponse = login("eve.holt@reqres.in");
        assertNegativeLoginResponse(errorResponse);
    }

}
