package com.example.api.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.example.api.steps.ExampleSteps.*;

@Epic("Allure Epic")
@Feature("Allure Feature")
public class ExampleTests {

    @Test
    @DisplayName("firstTest name")
    @Description("firstTest description")
    public void firstTest() {

        // comment
        firstStep();
        secondStep();
        thirdStep();
    }

    @Test
    @DisplayName("second name")
    @Description("second description")
    public void secondTest() {

        firstStep();
        secondStep();
        thirdStep();
    }

    @Test
    @DisplayName("second name")
    @Description("second description")
    public void second2Test() {

        firstStep();
        secondStep();
        thirdStep();
    }
}
